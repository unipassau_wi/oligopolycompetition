package edu.kit.exp.impl.continuousCompetition.server;

import edu.kit.exp.impl.continuousCompetition.client.ContinuousCompetitionParamObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by dschnurr on 02.10.14.
 */
public class ContinuousCompetitionMarketDataCalculator {

    private static final Logger log4j = LogManager.getLogger(ContinuousCompetitionProfitCalculator.class.getName());
    private boolean isTriopolyTreatment;
    private boolean isCournotTreatment;
    private boolean isQuadropolyTreatment;
    private static double theta;    //theta
    private static double a = 100;            //market size
    private static double b = 1.0;            //price elasticity
    private static double coeff_profit_tri_cournot  = (1.5625 / 60.0);
    private static double coeff_profit_tri_betrand  = (2.3625 / 60.0);
    private static double coeff_profit_duo_bertrand = (1.2500 / 60.0);
    private static double coeff_profit_duo_cournot = (1.0 / 60.0);
    private static double coeff_profit_quadro_cournot = (2.25 / 60.0);
    private static double coeff_profit_quadro_bertrand = ((16875.0/4375.0) / 60.0);

    private static double coeff_q_duo_cournot = (60.0/100.0);
    private static double coeff_q_trio_cournot = (300/700.0);
    private static double coeff_q_quadro_cournot = (1/3.0);

    public ContinuousCompetitionMarketDataCalculator(double diffParam, boolean isCournotTreatment, boolean isTriopolyTreatment) {
        this(diffParam, isCournotTreatment, isTriopolyTreatment, false);
    }


    public ContinuousCompetitionMarketDataCalculator(double diffParam, boolean isCournotTreatment, boolean isTriopolyTreatment, boolean isQuadropolyTreatment) {
        theta = (2.0/3.0);
        this.isCournotTreatment = isCournotTreatment;
        this.isTriopolyTreatment = isTriopolyTreatment;
        this.isQuadropolyTreatment = isQuadropolyTreatment;
    }


    public ContinuousCompetitionParamObject calculateMarketData(double pFirmA, double pFirmB, double pFirmC) {

        ContinuousCompetitionParamObject marketUpdate = calculateMarketData(pFirmA, pFirmB, pFirmC, -1);
        return marketUpdate;

    }


    public ContinuousCompetitionParamObject calculateMarketData(double pFirmA, double pFirmB, double pFirmC, double pFirmD) {
        ContinuousCompetitionParamObject marketUpdate;

        if (isQuadropolyTreatment) {
            if (isCournotTreatment) {
                marketUpdate = calculateMarketDataFromGivenQuantitiesForQuadropoly(pFirmA, pFirmB, pFirmC, pFirmD);
            } else {
                marketUpdate = calculateMarketDataFromGivenPricesForQuadropoly(pFirmA, pFirmB, pFirmC, pFirmD);
            }
        } else {
            if (isTriopolyTreatment) {
                if (isCournotTreatment) {
                    marketUpdate = calculateMarketDataFromGivenQuantitiesForTriopoly(pFirmA, pFirmB, pFirmC);
                } else {
                    marketUpdate = calculateMarketDataFromGivenPricesForTriopoly(pFirmA, pFirmB, pFirmC);
                }
            } else {
                // isDuopolyTreatment == true
                if (isCournotTreatment) {
                    marketUpdate = calculateMarketDataFromGivenQuantitiesForDuopoly(pFirmA, pFirmB);
                } else {
                    marketUpdate = calculateMarketDataFromGivenPricesForDuopoly(pFirmA, pFirmB);
                }
            }
        }


        return marketUpdate;
    }


    public ContinuousCompetitionParamObject calculateMarketDataFromGivenPricesForQuadropoly(double pFirmA, double pFirmB, double pFirmC, double pFirmD) {

        double pMarket;
        double profitFirmA, profitFirmB, profitFirmC, profitFirmD;
        double qFirmA, qFirmB, qFirmC, qFirmD, qMarket;

        FirmRecord firmA = new FirmRecord(pFirmA);
        FirmRecord firmB = new FirmRecord(pFirmB);
        FirmRecord firmC = new FirmRecord(pFirmC);
        FirmRecord firmD = new FirmRecord(pFirmD);

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(4), calcBeta(4), calcGamma(4));
        log4j.debug("Prices - pFirmA: {}, pFirmB: {}, pFirmC: {}, pFirmD: {}", pFirmA, pFirmB, pFirmC, pFirmD);

        pMarket = (pFirmA + pFirmB + pFirmC + pFirmD)/4;

        firmA.q = calculateDemandFromGivenPrices(pFirmA, 4, (pFirmB+pFirmC+pFirmD)/3);
        firmB.q = calculateDemandFromGivenPrices(pFirmB, 4, (pFirmA+pFirmC+pFirmD)/3);
        firmC.q = calculateDemandFromGivenPrices(pFirmC, 4, (pFirmA+pFirmB+pFirmD)/3);
        firmD.q = calculateDemandFromGivenPrices(pFirmD, 4, (pFirmA+pFirmB+pFirmC)/3);

        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}, qFirmC: {}, qFirmD: {}", firmA.q, firmB.q, firmC.q, firmD.q);

        FirmRecord[] firms = {firmA, firmB, firmC, firmD};
        Arrays.sort(firms);

        log4j.debug("Sorted Quantities - min: {}, second: {}, third: {}, max: {}", firms[0].q, firms[1].q, firms[2].q, firms[3].q);


        FirmRecord[] minFirm = {firms[0]};
        FirmRecord[] thiFirm = {firms[1]};
        FirmRecord[] secFirm = {firms[2]};
        FirmRecord[] maxFirm = {firms[3]};

        if (maxFirm[0].q < 0) {
            minFirm[0].q = 0.0;
            thiFirm[0].q = 0.0;
            secFirm[0].q = 0.0;
            maxFirm[0].q = 0.0;
        } else {
            // Demand of maxfirm is positive

            if (secFirm[0].q < 0) {
                // Demand of maxFirm is positive while other three firms exhibit negative demand
                // Calculate adjusted demand with n=4, m=1
                minFirm[0].q = 0.0;
                thiFirm[0].q = 0.0;
                secFirm[0].q = 0.0;

                maxFirm[0].q = calcAlpha(1) - calcBeta(1) * maxFirm[0].p;
                pMarket = maxFirm[0].p;

                log4j.debug("Retail Market: Three firm exit the market (Quadropoly calculation)");
            } else {
                // Demand of two firms (maxFirm, secFirm) is positive

                if (thiFirm[0].q < 0) {
                    // Demand of two firms is positive, while two firms exhibit negative demand
                    minFirm[0].q = 0.0;
                    thiFirm[0].q = 0.0;

                    // Calculate adjusted demand with n=4, m=2 for secFirm;
                    // Note that this demand may be negative for m=2 although it was positive for m=4
                    secFirm[0].q = calculateDemandFromGivenPricesInDuopoly(secFirm[0].p, maxFirm[0].p);

                    if (secFirm[0].q < 0) {
                        // Demand of secFirm is found to be negative in calculation for m=2
                        secFirm[0].q = 0.0;
                        maxFirm[0].q = calcAlpha(1) - calcBeta(1) * maxFirm[0].p;
                        pMarket = maxFirm[0].p;
                        log4j.debug("One firm remains active, three firms exit the market (Duopoly calculation)");
                    } else {
                        // Demand of secFirm is found to be positive in calculation for m=2
                        maxFirm[0].q = calculateDemandFromGivenPricesInDuopoly(maxFirm[0].p, secFirm[0].p);
                        pMarket = (maxFirm[0].p+secFirm[0].p)/2;
                        log4j.debug("Two firms remain active, two firm exit the market (Duopoly calculation)");
                    }

                } else {
                    // Demand of three firms is positive

                    if (minFirm[0].q < 0) {
                        // Demand of three firms is positive while one firm exhibits negative demand
                        minFirm[0].q = 0.0;

                        // Calculate adjusted demand with n=4, m=3 for thiFirm;
                        // Note that this demand may be negative for m=3 although it was positive for m=4
                        thiFirm[0].q = calculateDemandFromGivenPricesInTriopoly(thiFirm[0].p, secFirm[0].p, maxFirm[0].p);

                        if (thiFirm[0].q < 0) {
                            // Demand of thiFirm is found to be negative in calculation for m=3
                            thiFirm[0].q = 0.0;

                            // Calculate adjusted demand with n=4, m=2 for secFirm;
                            // Note that this demand may be negative for m=2 although it was positive for m=4
                            secFirm[0].q = calculateDemandFromGivenPricesInDuopoly(secFirm[0].p, maxFirm[0].p);

                            if (secFirm[0].q < 0) {
                                // Demand of secFirm is found to be negative in calculation for m=2
                                secFirm[0].q = 0.0;
                                maxFirm[0].q = calcAlpha(1) - calcBeta(1) * maxFirm[0].p;
                                pMarket = maxFirm[0].p;
                                log4j.debug("One firm remains active, three firms exit the market (Duopoly calculation)");
                            } else {
                                // Demand of secFirm is found to be positive in calculation for m=2
                                maxFirm[0].q = calculateDemandFromGivenPricesInDuopoly(maxFirm[0].p, secFirm[0].p);
                                pMarket = (maxFirm[0].p+secFirm[0].p)/2;
                                log4j.debug("Two firms remain active, two firm exit the market (Duopoly calculation)");
                            }

                        }  else {
                            // Demand of thiFirm is found to be positive in calculation for m=3
                            secFirm[0].q = calculateDemandFromGivenPricesInTriopoly(secFirm[0].p, thiFirm[0].p, maxFirm[0].p);
                            maxFirm[0].q = calculateDemandFromGivenPricesInTriopoly(maxFirm[0].p, thiFirm[0].p, secFirm[0].p);
                            pMarket = (maxFirm[0].p+secFirm[0].p+thiFirm[0].p)/3;
                            log4j.debug("Three firms remain active, one firm exits the market (Triopoly calculation)");
                        }
                    }
                    // Else: Demand for all four firms is positive, i.e. the inital demand values are valid
                }
            }
        }

        qFirmA = firmA.q;
        qFirmB = firmB.q;
        qFirmC = firmC.q;
        qFirmD = firmD.q;

        qMarket = (qFirmA + qFirmB +qFirmC+qFirmD)/4;

        profitFirmA = pFirmA * qFirmA * coeff_profit_quadro_bertrand;
        profitFirmB = pFirmB * qFirmB * coeff_profit_quadro_bertrand;
        profitFirmC = pFirmC * qFirmC * coeff_profit_quadro_bertrand;
        profitFirmD = pFirmD * qFirmD * coeff_profit_quadro_bertrand;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setpFirmC(pFirmC);
        marketUpdate.setpFirmD(pFirmD);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setqFirmC(qFirmC);
        marketUpdate.setqFirmD(qFirmD);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setProfitFirmC(profitFirmC);
        marketUpdate.setProfitFirmD(profitFirmD);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqMarket(qMarket);

        return marketUpdate;
    }

    public ContinuousCompetitionParamObject calculateMarketDataFromGivenQuantitiesForQuadropoly(double pFirmA, double pFirmB, double pFirmC, double pFirmD) {

        double pMarket;
        double profitFirmA, profitFirmB, profitFirmC, profitFirmD;
        double qFirmA, qFirmB, qFirmC, qFirmD;

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(4), calcBeta(4), calcGamma(4));
        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}, qFirmC: {}, qFirmD: {}", pFirmA, pFirmB, pFirmC, pFirmD);

        double pFirmAAdjusted = pFirmA * coeff_q_quadro_cournot;
        double pFirmBAdjusted = pFirmB * coeff_q_quadro_cournot;
        double pFirmCAdjusted = pFirmC * coeff_q_quadro_cournot;
        double pFirmDAdjusted = pFirmD * coeff_q_quadro_cournot;

        qFirmA = calculatePricesFromGivenQuantities(pFirmAAdjusted, (pFirmBAdjusted + pFirmCAdjusted + pFirmDAdjusted));
        qFirmB = calculatePricesFromGivenQuantities(pFirmBAdjusted, (pFirmAAdjusted + pFirmCAdjusted + pFirmDAdjusted));
        qFirmC = calculatePricesFromGivenQuantities(pFirmCAdjusted, (pFirmAAdjusted + pFirmBAdjusted + pFirmDAdjusted));
        qFirmD = calculatePricesFromGivenQuantities(pFirmDAdjusted, (pFirmAAdjusted + pFirmBAdjusted + pFirmCAdjusted));

        log4j.debug("Prices - pFirmA: {}, pFirmB: {}, pFirmC: {}, pFirmD: {}", qFirmA, qFirmB, qFirmC, qFirmD);

        pMarket = (pFirmA+pFirmB+pFirmC+pFirmD)/4;

        profitFirmA = pFirmAAdjusted * qFirmA * coeff_profit_quadro_cournot;
        profitFirmB = pFirmBAdjusted * qFirmB * coeff_profit_quadro_cournot;
        profitFirmC = pFirmCAdjusted * qFirmC * coeff_profit_quadro_cournot;
        profitFirmD = pFirmDAdjusted * qFirmD * coeff_profit_quadro_cournot;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setpFirmC(pFirmC);
        marketUpdate.setpFirmD(pFirmD);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setqFirmC(qFirmC);
        marketUpdate.setqFirmD(qFirmD);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setProfitFirmC(profitFirmC);
        marketUpdate.setProfitFirmD(profitFirmD);

        return marketUpdate;
    }

    public ContinuousCompetitionParamObject calculateMarketDataFromGivenPricesForTriopoly(double pFirmA, double pFirmB, double pFirmC) {

        double pMarket;
        double profitFirmA, profitFirmB, profitFirmC;
        double qFirmA, qFirmB, qFirmC, qMarket;

        FirmRecord firmA = new FirmRecord(pFirmA);
        FirmRecord firmB = new FirmRecord(pFirmB);
        FirmRecord firmC = new FirmRecord(pFirmC);

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(3), calcBeta(3), calcGamma(3));
        log4j.debug("Prices - pFirmA: {}, pFirmB: {}, pFirmC: {}", pFirmA, pFirmB, pFirmC);

        pMarket = (pFirmA + pFirmB + pFirmC)/3;

        firmA.q = calculateDemandFromGivenPricesInTriopoly(pFirmA, pFirmB, pFirmC);
        firmB.q = calculateDemandFromGivenPricesInTriopoly(pFirmB, pFirmA, pFirmC);
        firmC.q = calculateDemandFromGivenPricesInTriopoly(pFirmC, pFirmA, pFirmB);

        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}, qFirmC: {}", firmA.q, firmB.q, firmC.q);


        FirmRecord[] minFirm = {firmA};
        FirmRecord[] midFirm;
        FirmRecord[] maxFirm;
        if (firmB.q < firmA.q) {
            //qFirmA is updated to represent midFirm, qFirmB is set as new minFirm,
            midFirm = minFirm;
            minFirm = new FirmRecord[] {firmB};

        } else {
            //qFirmA is still minimum, qFirmB is set as midFirm
            midFirm = new FirmRecord[] {firmB};
        }

        if (firmC.q < minFirm[0].q) {
            maxFirm = midFirm;
            midFirm = minFirm;
            minFirm = new FirmRecord[] {firmC};
        } else {
            if (firmC.q < midFirm[0].q) {
                maxFirm = midFirm;
                midFirm = new FirmRecord[] {firmC};
            } else {
                maxFirm = new FirmRecord[] {firmC};
            }
        }


        if(maxFirm[0].q < 0) {
            // Retail demand < 0 for all firms, thus qFirm = 0 for all firms
            minFirm[0].q = 0;
            midFirm[0].q = 0;
            maxFirm[0].q = 0;
        } else {
            if (midFirm[0].q < 0) {
                // Retail demand of one firm is positive, while two firms exhibit negative demand.
                // Calculate adjusted demand with n=3, m=1 for firm that exhibits a positive demand in the case of m=n=3

                minFirm[0].q = 0.0;
                midFirm[0].q = 0.0;

                maxFirm[0].q = calcAlpha(1) - calcBeta(1) * maxFirm[0].p;
                pMarket = maxFirm[0].p;

                log4j.debug("Retail Market: Two firm exit the market (Triopoly calculation)");
            } else {
                if (minFirm[0].q < 0) {
                    // Retail demand of two firms is positive, while one firm exhibits negative demand.
                    // Calculate adjusted demand with n=3, m=2 for firms that exhibit a positive demand in the case of m=n=3

                    minFirm[0].q = 0.0;

                    midFirm[0].q = calcAlpha(2) - calcBeta(2) * midFirm[0].p + calcGamma(2) * (maxFirm[0].p);
                    log4j.debug("Duopoly calculation: midFirm.q: {}, midFirm.p: {}", midFirm[0].q, midFirm[0].p);

                    if (midFirm [0].q < 0) {
                        midFirm[0].q = 0.0;
                        maxFirm[0].q = calcAlpha(1) - calcBeta(1) * maxFirm[0].p;

                        pMarket = maxFirm[0].p;
                        log4j.debug("Retail Market: Two firm exit the market (Duopoly calculation)");
                    } else {
                        maxFirm[0].q = calcAlpha(2) - calcBeta(2) * maxFirm[0].p + calcGamma(2) * (midFirm[0].p);

                        pMarket = (maxFirm[0].p + midFirm[0].p)/2;
                        log4j.debug("Retail Market: One firm exits the market - (Positive quantities in duopoly)");
                        log4j.debug("Quantity check:"+minFirm[0].q+ " with price "+minFirm[0].p);
                    }

                }
                // else: qFirm > 0 for all firms, thus initial demand is valid
            }

        }

        qFirmA = firmA.q;
        qFirmB = firmB.q;
        qFirmC = firmC.q;

        qMarket = (qFirmA + qFirmB +qFirmC)/3;

        profitFirmA = pFirmA * qFirmA * coeff_profit_tri_betrand;
        profitFirmB = pFirmB * qFirmB * coeff_profit_tri_betrand;
        profitFirmC = pFirmC * qFirmC * coeff_profit_tri_betrand;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setpFirmC(pFirmC);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setqFirmC(qFirmC);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setProfitFirmC(profitFirmC);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqMarket(qMarket);

        return marketUpdate;
    }

    public ContinuousCompetitionParamObject calculateMarketDataFromGivenPricesForDuopoly(double pFirmA, double pFirmB) {

        double pMarket, qMarket;
        double profitFirmA, profitFirmB;
        double qFirmA, qFirmB;

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(3), calcBeta(3), calcGamma(3));
        log4j.debug("Prices - pFirmA: {}, pFirmB: {}", pFirmA, pFirmB);

        pMarket = (pFirmA + pFirmB)/2;
        qFirmA = calculateDemandFromGivenPricesInDuopoly(pFirmA, pFirmB);
        qFirmB = calculateDemandFromGivenPricesInDuopoly(pFirmB, pFirmA);

        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}", qFirmA, qFirmB);


        if (qFirmA < 0) {
            // firmB is only firm in market
            qFirmA = 0.0;
            qFirmB = calcAlpha(1) - calcBeta(1) * pFirmB;
            pMarket = pFirmB;
            log4j.debug("Duopoly calculation: firmA exits the market - qFirmB: {}, pFirmB: {}", qFirmB, pFirmB);
        } else {
            if (qFirmB < 0) {
                // firm A is only firm in market
                qFirmB = 0.0;
                qFirmA = calcAlpha(1) - calcBeta(1) * pFirmA;
                pMarket = pFirmA;
                log4j.debug("Duopoly calculation: firmB exits the market - qFirmA: {}, pFirmA: {}", qFirmA, pFirmA);
            }
            // else duopoly quantities are valid
        }

        qMarket = (qFirmA + qFirmB)/2;

        profitFirmA = pFirmA * qFirmA * coeff_profit_duo_bertrand;
        profitFirmB = pFirmB * qFirmB * coeff_profit_duo_bertrand;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqMarket(qMarket);

        marketUpdate.setpFirmC(0.0);
        marketUpdate.setqFirmC(0.0);
        marketUpdate.setProfitFirmC(0.0);

        return marketUpdate;
    }

    public ContinuousCompetitionParamObject calculateMarketDataFromGivenQuantitiesForTriopoly(double pFirmA, double pFirmB, double pFirmC) {

        double pMarket;
        double profitFirmA, profitFirmB, profitFirmC;
        double qFirmA, qFirmB, qFirmC;

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(3), calcBeta(3), calcGamma(3));
        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}, qFirmC: {}", pFirmA, pFirmB, pFirmC);

        double pFirmAAdjusted = pFirmA * coeff_q_trio_cournot;
        double pFirmBAdjusted = pFirmB * coeff_q_trio_cournot;
        double pFirmCAdjusted = pFirmC * coeff_q_trio_cournot;

        qFirmA = calculatePricesFromGivenQuantitiesInTriopoly(pFirmAAdjusted, pFirmBAdjusted, pFirmCAdjusted);
        qFirmB = calculatePricesFromGivenQuantitiesInTriopoly(pFirmBAdjusted, pFirmAAdjusted, pFirmCAdjusted);
        qFirmC = calculatePricesFromGivenQuantitiesInTriopoly(pFirmCAdjusted, pFirmAAdjusted, pFirmBAdjusted);
        log4j.debug("Prices - pFirmA: {}, pFirmB: {}, pFirmC: {}", qFirmA, qFirmB, qFirmC);

        pMarket = (pFirmA+pFirmB+pFirmC)/3;

        profitFirmA = pFirmAAdjusted * qFirmA * coeff_profit_tri_cournot;
        profitFirmB = pFirmBAdjusted * qFirmB * coeff_profit_tri_cournot;
        profitFirmC = pFirmCAdjusted * qFirmC * coeff_profit_tri_cournot;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setpFirmC(pFirmC);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setqFirmC(qFirmC);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setProfitFirmC(profitFirmC);

        return marketUpdate;

    }

    public ContinuousCompetitionParamObject calculateMarketDataFromGivenQuantitiesForDuopoly(double pFirmA, double pFirmB) {

        double pMarket, qMarket;
        double profitFirmA, profitFirmB;
        double qFirmA, qFirmB;

        log4j.debug("Params - theta: {}, alpha: {}, beta: {}, gamma: {}", theta, calcAlpha(3), calcBeta(3), calcGamma(3));
        log4j.debug("Quantities - qFirmA: {}, qFirmB: {}", pFirmA, pFirmB);

        double pFirmAAdjusted = pFirmA * coeff_q_duo_cournot;
        double pFirmBAdjusted = pFirmB * coeff_q_duo_cournot;

        qFirmA = calculatePricesFromGivenQuantitiesInDuopoly(pFirmAAdjusted, pFirmBAdjusted);
        qFirmB = calculatePricesFromGivenQuantitiesInDuopoly(pFirmBAdjusted, pFirmAAdjusted);

        log4j.debug("Prices - pFirmA: {}, pFirmB: {}", qFirmA, qFirmB);

        pMarket = (pFirmA + pFirmB)/2;
        qMarket = (qFirmA + qFirmB)/2;

        profitFirmA = pFirmAAdjusted * qFirmA * coeff_profit_duo_cournot;
        profitFirmB = pFirmBAdjusted * qFirmB * coeff_profit_duo_cournot;

        ContinuousCompetitionParamObject marketUpdate = new ContinuousCompetitionParamObject();

        marketUpdate.setpFirmA(pFirmA);
        marketUpdate.setpFirmB(pFirmB);
        marketUpdate.setqFirmA(qFirmA);
        marketUpdate.setqFirmB(qFirmB);
        marketUpdate.setProfitFirmA(profitFirmA);
        marketUpdate.setProfitFirmB(profitFirmB);
        marketUpdate.setpMarket(pMarket);
        marketUpdate.setqMarket(qMarket);

        marketUpdate.setpFirmC(0.0);
        marketUpdate.setqFirmC(0.0);
        marketUpdate.setProfitFirmC(0.0);

        return marketUpdate;
    }

    private double calculatePricesFromGivenQuantitiesInDuopoly(double q, double qOtherFirm) {
        return (a - b * (q + theta * qOtherFirm));
    }

    private double calculatePricesFromGivenQuantitiesInTriopoly(double q, double qOtherFirm1, double qOtherFirm2) {
        return (a - b * (q + theta * (qOtherFirm1 + qOtherFirm2)));
    }

    private double calculatePricesFromGivenQuantities(double q, double Q) {
        return (a - b * (q + theta * Q));
    }

    private double calculateDemandFromGivenPrices(double p, double n, double pAverageOtherFirms) {
        return (calcAlpha(n) - calcBeta(n) * p + calcGamma(n) * (pAverageOtherFirms));
    }


    private double calculateDemandFromGivenPricesInTriopoly(double p, double pOtherFirm1, double pOtherFirm2) {
        return (calcAlpha(3) - calcBeta(3) * p + calcGamma(3) * ((pOtherFirm1+pOtherFirm2)/2));
    }

    private double calculateDemandFromGivenPricesInDuopoly(double p, double pOtherFirm) {
        return (calcAlpha(2) - calcBeta(2) * p + calcGamma(2) * pOtherFirm);
    }

    private double calcAlpha(double n) {
        return (a / (b * (1 + (n-1) * theta)));
    }

    private double calcBeta(double n) {
        return ((1 + (n-2) * theta) / (b * (1-theta) * (1 + (n-1) * theta)));
    }

    private double calcGamma(double n) {
        return (((n-1) * theta) / (b * (1-theta) * (1 + (n-1) * theta)));
    }

    /* Helper class to store (price, quantity) pairs */
    class FirmRecord implements Comparable<FirmRecord> {
        double p;
        double q;

        public FirmRecord (double p) {
            this.p = p;
        }

        public FirmRecord (double q, double p) {
            this.q = q;
            this.p = p;
        }

        @Override
        public int compareTo(FirmRecord o) {

            if (this.q < o.q) {
                return -1;
            }

            if (this.q > o.q) {
                return 1;
            }

            return 0;
        }
    }

}
