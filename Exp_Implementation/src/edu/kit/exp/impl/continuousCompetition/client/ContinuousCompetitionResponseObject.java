package edu.kit.exp.impl.continuousCompetition.client;

import edu.kit.exp.client.gui.screens.Screen;

/**
 * Created by dschnurr on 05.03.14.
 */
public class ContinuousCompetitionResponseObject extends Screen.ResponseObject {

    private static final double COEFFICIENT_PRICES = 1;

    private String statusMsg;
    private int localTime;
    private int countId;
    private boolean clientReady = false;
    private boolean clientFinished = false;
    private int roleCode;
    private boolean practiceRoundFinished = false;

    private double pFirmA;
    private double pFirmB;
    private double pFirmC;
    private double pFirmD;

    private double balanceFirmA;
    private double balanceFirmB;
    private double balanceFirmC;
    private double balanceFirmD;

    // Setter methods

    public void setPracticeRoundFinished(boolean practiceRoundFinished) {
        this.practiceRoundFinished = practiceRoundFinished;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setLocalTime(int localTime) {
        this.localTime = localTime;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    public void setpFirmA(int pFirmA) {
        this.pFirmA = ((double) pFirmA) / COEFFICIENT_PRICES;
    }

    public void setpFirmB(int pFirmB) {
        this.pFirmB = ((double) pFirmB) / COEFFICIENT_PRICES;
    }

    public void setpFirmC(int pFirmC) {
        this.pFirmC = ((double) pFirmC) / COEFFICIENT_PRICES;
    }

    public void setpFirmD(int pFirmD) {
        this.pFirmD = ((double) pFirmD) / COEFFICIENT_PRICES;
    }

    public void setBalanceFirmA(double balanceFirmA) {
        this.balanceFirmA = balanceFirmA;
    }

    public void setBalanceFirmB(double balanceFirmB) {
        this.balanceFirmB = balanceFirmB;
    }

    public void setBalanceFirmC(double balanceFirmC) {
        this.balanceFirmC = balanceFirmC;
    }

    public void setBalanceFirmD(double balanceFirmD) {
        this.balanceFirmD = balanceFirmD;
    }

    public void setClientFinished(boolean clientFinished) {
        this.clientFinished = clientFinished;
    }


    // Getter methods

    public boolean isPracticeRoundFinished() {
        return practiceRoundFinished;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public int getLocalTime() {
        return localTime;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public double getpFirmA() {
        return pFirmA;
    }

    public double getpFirmB() {
        return pFirmB;
    }

    public double getpFirmC() {
        return pFirmC;
    }

    public double getpFirmD() {
        return pFirmD;
    }

    public int getCountId() {
        return countId;
    }

    public void setCountId(int countId) {
        this.countId = countId;
    }

    public boolean isClientReady() {
        return clientReady;
    }

    public void setClientReady(boolean clientReady) {
        this.clientReady = clientReady;
    }

    public double getBalanceFirmA() {
        return balanceFirmA;
    }

    public double getBalanceFirmB() {
        return balanceFirmB;
    }

    public double getBalanceFirmC() {
        return balanceFirmC;
    }

    public boolean isClientFinished() {
        return clientFinished;
    }
}
