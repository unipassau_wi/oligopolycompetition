package edu.kit.exp.impl.continuousCompetition.client;

import edu.kit.exp.client.gui.screens.Screen.ParamObject;
import edu.kit.exp.server.jpa.entity.Subject;
import edu.kit.exp.server.jpa.entity.SubjectGroup;

/**
 * Created by dschnurr on 04.03.14.
 */
public class ContinuousCompetitionParamObject extends ParamObject {

    SubjectGroup subjectGroup;
    Subject subject;
    String screenName;
    double diffParam;
    boolean practiceRound = false;
    boolean initialUpdate = false;
    boolean isDiscreteTreatment = false;
    boolean isTriopolyTreatment = true;
    boolean isCournotTreatment;
    boolean isQuadropolyTreatment;
    private int duration;
    private int globalTime;
    private int updateTimeStep;

    private final double COEFFICIENT_PRICES = 1;
    private final double COEFFICIENT_PROFITS = 1;


    private String statusMsg;

    private Boolean startFlag = false;
    private FirmDescription role;
    int roleCode;
    int countId;

    private double pFirmA;
    private double pFirmB;
    private double pFirmC;
    private double pFirmD;

    private double pMarket;
    private double qMarket;

    private double qFirmA;
    private double qFirmB;
    private double qFirmC;
    private double qFirmD;

    private double profitFirmA;
    private double profitFirmB;
    private double profitFirmC;
    private double profitFirmD;

    private double balanceFirmA;
    private double balanceFirmB;
    private double balanceFirmC;
    private double balanceFirmD;


    public void setDiffParam(double diffParam) {
        this.diffParam = diffParam;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setUpdateTimeStep(int updateTimeStep) {
        this.updateTimeStep = updateTimeStep;
    }

    public void setPracticeRound(boolean practiceRound) {
        this.practiceRound = practiceRound;
    }

    public void setInitialUpdate(boolean initialUpdate) {
        this.initialUpdate = initialUpdate;
    }

    public void setSubjectGroup(SubjectGroup subjectGroup) {
        this.subjectGroup = subjectGroup;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public void setRole(FirmDescription role) {
        this.role = role;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    public void setDiscreteTreatment(boolean isDiscreteTreatment) {
        this.isDiscreteTreatment = isDiscreteTreatment;
    }

    public void setTriopolyTreatment(boolean isDuopolyTreatment) {
        this.isTriopolyTreatment = isDuopolyTreatment;
    }

    public void setQuadropolyTreatment(boolean isQuadropolyTreatment) {
        this.isQuadropolyTreatment = isQuadropolyTreatment;
    }

    public void setCournotTreatment(boolean isCournotTreatment) {
        this.isCournotTreatment = isCournotTreatment;
    }

    public void setpFirmA(double pFirmA) {
        this.pFirmA = pFirmA;
    }

    public void setpFirmB(double pFirmB) {
        this.pFirmB = pFirmB;
    }

    public void setpFirmC(double pFirmC) {
        this.pFirmC = pFirmC;
    }

    public void setpFirmD(double pFirmD) {
        this.pFirmD = pFirmD;
    }

    public void setpMarket(double pMarket) {
        this.pMarket = pMarket;
    }

    public void setqMarket(double qMarket) {
        this.qMarket = qMarket;
    }

    public void setProfitFirmA(double profitFirmA) {
        this.profitFirmA = profitFirmA;
    }

    public void setProfitFirmB(double profitFirmB) {
        this.profitFirmB = profitFirmB;
    }

    public void setProfitFirmC(double profitFirmC) {
        this.profitFirmC = profitFirmC;
    }

    public void setProfitFirmD(double profitFirmD) {
        this.profitFirmD = profitFirmD;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setGlobalTime(int globalTime) {
        this.globalTime = globalTime;
    }

    public void setqFirmA(double qFirmA) {
        this.qFirmA = qFirmA;
    }

    public void setqFirmB(double qFirmB) {
        this.qFirmB = qFirmB;
    }

    public void setqFirmC(double qFirmC) {
        this.qFirmC = qFirmC;
    }

    public void setqFirmD(double qFirmD) {
        this.qFirmD = qFirmD;
    }

    public void setBalanceFirmA(double balanceFirmA) {
        this.balanceFirmA = balanceFirmA;
    }

    public void setBalanceFirmB(double balanceFirmB) {
        this.balanceFirmB = balanceFirmB;
    }

    public void setBalanceFirmC(double balanceFirmC) {
        this.balanceFirmC = balanceFirmC;
    }

    public void setBalanceFirmD(double balanceFirmD) {
        this.balanceFirmD = balanceFirmD;
    }



    public boolean isInitialUpdate() {
        return initialUpdate;
    }

    public int getDuration() {
        return duration;
    }

    public int getUpdateTimeStep() {
        return updateTimeStep;
    }

    public boolean isPracticeRound() {
        return practiceRound;
    }

    public boolean isDiscreteTreatment() {
        return isDiscreteTreatment;
    }

    public boolean isTriopolyTreatment() {
        return isTriopolyTreatment;
    }

    public boolean isCournotTreatment() {
        return isCournotTreatment;
    }

    public double getDiffParam() {
        return diffParam;
    }

    public SubjectGroup getSubjectGroup() {
        return subjectGroup;
    }

    public Subject getSubject() {
        return subject;
    }

    public String getScreenName() {
        return screenName;
    }

    public FirmDescription getRole() {
        return role;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public int getGlobalTime() {
        return globalTime;
    }

    public int getpFirmA() {
        return (int) Math.round(pFirmA*COEFFICIENT_PRICES);
    }

    public int getpFirmB() {
        return (int) Math.round(pFirmB*COEFFICIENT_PRICES);
    }

    public int getpFirmC() {
        return (int) Math.round(pFirmC*COEFFICIENT_PRICES);
    }

    public int getpFirmD() {
        return (int) Math.round(pFirmD*COEFFICIENT_PRICES);
    }

    public double getpMarket() {
        return (pMarket*COEFFICIENT_PRICES);
    }

    public double getqMarket() {
        return qMarket;
    }

    public double getProfitFirmA() {
        return (profitFirmA*COEFFICIENT_PROFITS);
    }

    public double getProfitFirmB() {
        return (profitFirmB*COEFFICIENT_PROFITS);
    }

    public double getProfitFirmC() {
        return (profitFirmC*COEFFICIENT_PROFITS);
    }

    public double getProfitFirmD() {
        return (profitFirmD*COEFFICIENT_PROFITS);
    }

    public Boolean getStartFlag() {
        return startFlag;
    }

    public void setStartFlag(Boolean startFlag) {
        this.startFlag = startFlag;
    }

    public int getCountId() {
        return countId;
    }

    public void setCountId(int countId) {
        this.countId = countId;
    }

    public double getqFirmA() {
        return qFirmA;
    }

    public double getqFirmB() {
        return qFirmB;
    }

    public double getqFirmC() {
        return qFirmC;
    }

    public double getqFirmD() {
        return qFirmD;
    }

    public double getBalanceFirmA() {
        return balanceFirmA;
    }

    public double getBalanceFirmB() {
        return balanceFirmB;
    }

    public double getBalanceFirmC() {
        return balanceFirmC;
    }

    public double getBalanceFirmD() {
        return balanceFirmD;
    }
}
