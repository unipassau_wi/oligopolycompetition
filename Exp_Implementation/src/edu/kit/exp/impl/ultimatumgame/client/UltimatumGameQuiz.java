package edu.kit.exp.impl.ultimatumgame.client;

import edu.kit.exp.client.gui.screens.quiz.QuizItemNumberInput;
import edu.kit.exp.client.gui.screens.quiz.QuizItemMultipleChoice;
import edu.kit.exp.client.gui.screens.quiz.QuizScreen;

public class UltimatumGameQuiz extends QuizScreen {

	private static final long serialVersionUID = -8841977439730389225L;

	public UltimatumGameQuiz(String gameId, ParamObject parameter, String screenId, Long showUpTime) {
		super(gameId, parameter, screenId, showUpTime);
	
		QuizItemMultipleChoice question;
		question = new QuizItemMultipleChoice("A simple question with one correct answer?");
		question.addAnswer("2");
		question.addAnswer("4 (true)", true);
		question.addAnswer("8");
		question.addAnswer("10");
		this.addQuizItem(question);
		
		question = new QuizItemMultipleChoice("A simple question with more correct answers?");
		question.addAnswer("2");
		question.addAnswer("3 (true)", true);
		question.addAnswer("6 (true, too)", true);
		question.addAnswer("9");
		this.addQuizItem(question);
		
		question = new QuizItemMultipleChoice("A simple question with one correct answer but multiple options?");
		question.setSelectMultiple(true);
		question.addAnswer("2");
		question.addAnswer("3 (only this is true)", true);
		question.addAnswer("6");
		question.addAnswer("9");
		this.addQuizItem(question);
		
		QuizItemNumberInput inputQuestion = new QuizItemNumberInput("A simple question with an integer input? (5.447 is correct)", 5.447);
		this.addQuizItem(inputQuestion);
		
		inputQuestion = new QuizItemNumberInput("A simple question with an integer input? (5 is correct)", 5);
		this.addQuizItem(inputQuestion);
	}
}
