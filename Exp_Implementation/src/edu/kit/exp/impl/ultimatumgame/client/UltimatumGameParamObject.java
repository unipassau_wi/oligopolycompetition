/**
 * 
 */
package edu.kit.exp.impl.ultimatumgame.client;

import edu.kit.exp.client.gui.screens.Screen.ParamObject;

/**
 * @author jf9172
 *
 */
public class UltimatumGameParamObject extends ParamObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5545747155672141767L;
	
	private String infoText;
	private String inputValue;
	private Boolean actionPerformed;
	
	public String getInfoText() {
		return infoText;
	}

	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}

	public String getInputValue() {
		return inputValue;
	}

	public void setInputValue(String inputValue) {
		this.inputValue = inputValue;
	}

	public Boolean getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(Boolean actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

}
