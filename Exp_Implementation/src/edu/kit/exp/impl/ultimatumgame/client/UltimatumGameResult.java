package edu.kit.exp.impl.ultimatumgame.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.kit.exp.client.gui.screens.Screen;

/**
 * The ultimatum game result screen.
 * 
 */
public class UltimatumGameResult extends Screen {

	private static final long serialVersionUID = -5584915447530697658L;
	private String message;
	private JLabel labelInfo;

	/**
	 * Creates am instance of DefaultInfoScreen.
	 * 
	 * @param queueId
	 * @param parameter
	 *            Screen parameter Index: 0=result message, 1=show up time
	 * @param showUpTime
	 */
	public UltimatumGameResult(String queueId, UltimatumGameParamObject parameter, String screenId, Long showUpTime) {
		super(queueId, parameter, screenId, showUpTime);

		setLogDisplayEvent(true);

		this.message = (String) parameter.getInfoText();
		setLayout(new BorderLayout(0, 0));
		this.labelInfo = new JLabel(message);
		labelInfo.setFont(new Font("Tahoma", Font.BOLD, 17));

		setBorder(new EmptyBorder(10, 10, 10, 10));
		setBackground(Color.WHITE);

		setLayout(new BorderLayout(0, 0));

		JPanel infoPanel = new JPanel();
		infoPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		infoPanel.setBackground(Color.WHITE);
		add(infoPanel, BorderLayout.CENTER);
		infoPanel.setLayout(new BorderLayout(0, 0));
		infoPanel.add(labelInfo);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(UltimatumGameResult.class.getResource("/edu/kit/exp/common/resources/kit_logo.png")));
		panel.add(lblLogo, BorderLayout.EAST);
	}

	public void setCustomScreenParameter(ArrayList<Object> parameters) throws RemoteException {

		// welcome message text from server
		this.message = (String) parameters.get(0);

	}
}
