package edu.kit.exp.impl.ultimatumgame.server;

import java.util.ArrayList;
import java.util.List;

import edu.kit.exp.server.jpa.entity.Period;
import edu.kit.exp.server.jpa.entity.Subject;
import edu.kit.exp.server.microeconomicsystem.RoleMatcher;
import edu.kit.exp.server.run.RandomGeneratorException;
import edu.kit.exp.server.run.RandomNumberGenerator;

/**
 * This class matches the 2 given roles to subjects randomly. The roles will be
 * switched every period.
 * 
 */
public class UltimatumGameRoleMatcher extends RoleMatcher {

	private RandomNumberGenerator randomNumberGenerator = RandomNumberGenerator.getInstance();

	public UltimatumGameRoleMatcher(List<String> roles) {
		super(roles);

	}

	/**
	 * Matches the 2 given roles to subjects randomly.
	 */
	@Override
	public List<Subject> setupSubjectRoles(List<Subject> subjects) throws RandomGeneratorException {

		int numberOfSubjects = subjects.size();
		int max = numberOfSubjects - 1;
		int min = 0;
		int numberOfRoles = roles.size();

		int copiesPerRole = numberOfSubjects / numberOfRoles;

		ArrayList<String> bowle = new ArrayList<String>();

		// Add roles to bowle
		for (String role : roles) {

			for (int i = 0; i < copiesPerRole; i++) {
				bowle.add(role);
			}
		}

		ArrayList<Integer> randomNumbers = randomNumberGenerator.generateNonRepeatingIntegers(min, max);

		Subject subject = null;
		int roleCopyNumber;

		for (int index = 0; index < subjects.size(); index++) {

			subject = subjects.get(index);
			roleCopyNumber = randomNumbers.get(index);
			subject.setRole(bowle.get(roleCopyNumber));
		}

		return subjects;

	}

	/**
	 * Switches the roles of all subjects.
	 */
	@Override
	public List<Subject> rematch(Period period, List<Subject> subjects) throws RandomGeneratorException {

		for (Subject subject : subjects) {

			if (subject.getRole().equals(roles.get(0))) {
				subject.setRole(roles.get(1));
			} else {
				subject.setRole(roles.get(0));
			}
		}

		return subjects;
	}

}
