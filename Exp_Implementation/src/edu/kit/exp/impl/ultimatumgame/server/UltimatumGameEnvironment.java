package edu.kit.exp.impl.ultimatumgame.server;

import edu.kit.exp.server.microeconomicsystem.Environment;
import edu.kit.exp.server.microeconomicsystem.PartnerMatcher;
import edu.kit.exp.server.microeconomicsystem.RoleMatcher;
import edu.kit.exp.server.microeconomicsystem.SubjectGroupMatcher;


/** 
 * This Class represents the microeconomic environment in an ultimatum game
 *
 */
public class UltimatumGameEnvironment extends Environment{
	
	//Roles
	private static String ROLE_REQUESTER = "Requester";
	private static String ROLE_RESPONDER = "Responder";
	
	//money to share
	private Integer moneyToShare = 10;
	
	public UltimatumGameEnvironment() {		
					
		this.roles.add(ROLE_REQUESTER);
		this.roles.add(ROLE_RESPONDER);
		this.roleMatcher = new UltimatumGameRoleMatcher(roles);
		this.subjectGroupMatcher = new PartnerMatcher(roles);
		this.setResetMatchersAfterTreatmentBlocks(false);
	}

	@Override
	public RoleMatcher getRoleMatcher() {
		
		return this.roleMatcher;
	}

	@Override
	public SubjectGroupMatcher getSubjectGroupMatcher() {
	
		return this.subjectGroupMatcher;
	}

	public Integer getMoneyToShare() {
		return moneyToShare;
	}

	public void setMoneyToShare(Integer moneyToShare) {
		this.moneyToShare = moneyToShare;
	}
	
	

	

	
	
	
	
}

