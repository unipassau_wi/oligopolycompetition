package edu.kit.exp.impl.ultimatumgame.server;

import java.util.Date;
import java.util.List;

import edu.kit.exp.server.communication.ClientResponseMessage;
import edu.kit.exp.server.communication.ServerMessageSender;
import edu.kit.exp.server.jpa.entity.Membership;
import edu.kit.exp.server.jpa.entity.Subject;
import edu.kit.exp.server.jpa.entity.Trial;
import edu.kit.exp.server.microeconomicsystem.Institution;
import edu.kit.exp.server.structure.SubjectManagement;
import edu.kit.exp.server.structure.TrialManagement;
import edu.kit.exp.client.gui.screens.*;
import edu.kit.exp.impl.ultimatumgame.client.UltimatumGameParamObject;
import edu.kit.exp.impl.ultimatumgame.client.UltimatumGameRequest;
import edu.kit.exp.impl.ultimatumgame.client.UltimatumGameResponse;
import edu.kit.exp.impl.ultimatumgame.client.UltimatumGameResult;

/**
 * This class implements an ultimatum game institution
 * 
 */
public class UltimatumGameInstitution extends Institution<UltimatumGameEnvironment> {

	private Subject requester;
	private Subject responder;
	private UltimatumGameEnvironment ultimatumGameEnvironment;
	private Integer requesterShare;
	private Integer responderShare;
	private String screenId;

	public UltimatumGameInstitution(UltimatumGameEnvironment environment, List<Membership> memberships, ServerMessageSender messageSender, String gameId) {
		super(environment, memberships, messageSender, gameId);

		this.ultimatumGameEnvironment = environment;

	}

	@Override
	public void startPeriod() throws Exception {

		System.out.println("Institution started");

		// Find requester and responder
		for (Membership membership : memberships) {
			if (membership.getRole().equals("Responder")) {
				responder = membership.getSubject();
			} else {
				requester = membership.getSubject();
			}
		}

		// Requester gets a budget which he has to divide. Responder has to
		// wait.
		UltimatumGameParamObject parameterRequester = new UltimatumGameParamObject();
		parameterRequester.setInfoText("<html><bod><h1>You are requester!</h1>You own " + ultimatumGameEnvironment.getMoneyToShare() + " Euro and you have to share it with an other Agent.<br>How much do you offer to the other agent?");

		showScreen(requester, UltimatumGameRequest.class, parameterRequester);
		showScreen(responder, DefaultWaitingScreen.class, new DefaultWaitingScreen.ParamObject());

	}

	@Override
	public void processMessage(ClientResponseMessage msg) throws Exception {

		// read message
		Long clientTimeStamp = msg.getClientTimeStamp();
		Long serverTimeStamp = msg.getServerTimeStamp();
		String clientId = msg.getClientId();
		UltimatumGameParamObject parameter = msg.getParameters();
		screenId = msg.getScreenId();
		
	
		System.out.println("Institution received mesage: " + msg.toString());
		String sender = clientId;

		// Institution received offer requester
		if (sender.equals(requester.getIdClient())) {

			int offer = Integer.valueOf(parameter.getInputValue());

			// Create new Trial of requesters offer and save it in DB
			Trial trial = new Trial();
			trial.setSubjectGroup(getSubjectGroup());
			trial.setValue(String.valueOf(offer));
			trial.setEvent("OFFER");
			trial.setSubject(requester);
			trial.setClientTime(clientTimeStamp);
			trial.setServerTime(serverTimeStamp);
			trial.setScreenName(screenId);

			TrialManagement.getInstance().createNewTrial(trial);

			responderShare = offer;
			requesterShare = ultimatumGameEnvironment.getMoneyToShare() - offer;
			UltimatumGameParamObject sendParameter = new UltimatumGameParamObject();
			String message = String.valueOf("<html><body><h1>You are responder!</h1>The requester offers you " + String.valueOf(offer) + " Euro of " + ultimatumGameEnvironment.getMoneyToShare() + " Euro! <br>Do you accept this offer?" + "</body></html>");
			sendParameter.setInfoText(message);

			// Send offer to responder (taker)
			showScreen(responder, UltimatumGameResponse.class, sendParameter);
			showScreen(requester, DefaultWaitingScreen.class, new DefaultWaitingScreen.ParamObject());

		} else {

			// Institution received response of responder
			System.out.println("Institution received responder mesage: " + msg.toString());
			Boolean acceptance = ((UltimatumGameParamObject) msg.getParameters()).getActionPerformed();

			// Create new Trial of responders answer and save it in DB
			Trial trial = new Trial();
			trial.setSubjectGroup(getSubjectGroup());
			trial.setValue(acceptance.toString());
			trial.setEvent("RESPONSE");
			trial.setSubject(responder);
			trial.setClientTime(clientTimeStamp);
			trial.setServerTime(serverTimeStamp);
			trial.setScreenName(screenId);

			TrialManagement.getInstance().createNewTrial(trial);

			// Did responder accept?
			if (!acceptance) {
				requesterShare = 0;
				responderShare = 0;
			}

			endPeriod();
		}

	}

	@Override
	protected void endPeriod() throws Exception {

		// Calculate total payoffs
		requester.setPayoff(requester.getPayoff() + requesterShare);
		responder.setPayoff(responder.getPayoff() + responderShare);

		SubjectManagement.getInstance().updateSubject(requester);
		SubjectManagement.getInstance().updateSubject(responder);

		// Create new Trials to log the payoffs in DB
		Trial trialResponder = new Trial();
		trialResponder.setSubjectGroup(getSubjectGroup());
		trialResponder.setValue(responder.getPayoff().toString());
		trialResponder.setScreenName(screenId);
		trialResponder.setEvent("PAYOFF");
		trialResponder.setSubject(responder);
		trialResponder.setServerTime(new Date().getTime());
		TrialManagement.getInstance().createNewTrial(trialResponder);

		Trial trialRequester = new Trial();
		trialRequester.setSubjectGroup(getSubjectGroup());
		trialRequester.setValue(requester.getPayoff().toString());
		trialRequester.setScreenName(screenId);
		trialRequester.setEvent("PAYOFF");
		trialRequester.setSubject(requester);
		trialRequester.setServerTime(new Date().getTime());
		TrialManagement.getInstance().createNewTrial(trialRequester);

		// Result message --> Display time will be logged at client side
		String message = String.valueOf("<html><body><h1>Result</h1>Requester gets: " + requesterShare + " Euro.<br>Responder gets: " + responderShare + " Euro.</body></html>");
		UltimatumGameParamObject sendParameter = new UltimatumGameParamObject();
		sendParameter.setInfoText(message);

		// show result to subjects for 5 seconds
		showScreenWithDeadLine(responder, UltimatumGameResult.class, sendParameter, 5000L);
		showScreenWithDeadLine(requester, UltimatumGameResult.class, sendParameter, 5000L);

		// Wait 1 minute 
		message = String.valueOf("Wait 1 minute between periods...");
		sendParameter = new UltimatumGameParamObject();
		sendParameter.setInfoText(message);

		showScreenWithDeadLine(responder, DefaultWaitingScreen.class, new DefaultWaitingScreen.ParamObject(), 3000L);
		showScreenWithDeadLine(requester, DefaultWaitingScreen.class, new DefaultWaitingScreen.ParamObject(), 3000L);

		// End period
		this.finished = true;

	}

}
